" Preamble {{{
" ==============================================================================
"        File: MasterWindow.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

if exists('g:TWM#MasterWindow#autoload')
  finish
endif

if v:version < 700
  echoerr "TWM: Error: Minimum required version of Vim is \"7.0.0\"."
  finish
endif

let g:TWM#MasterWindow#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ==============================================================================
" Public {{{
" ==============================================================================
function! TWM#MasterWindow#SetWidth(width)
  if     type(a:width) == type(0.0)
    let l:width = float2nr(a:width)
  elseif type(a:width) == type(0)
    let l:width = a:width
  elseif type(a:width) == type('')
    let l:width = str2nr(a:width)
  else
    throw
    \ "`a:width` should be one of types {`Float`, `Number`, `String`}, and only"
    \ ' '
    \ "values between \"0\" and `&columns` make sense."
  endif
  if winnr('$') == 1
    return
  endif
  let l:width  = s:Clamp(l:width, 0, l:width)
  let l:window = winnr()
  let  l:lazyredraw = &l:lazyredraw
  let &l:lazyredraw = 1
  try
    silent! noautocmd 1wincmd w
    silent! noautocmd execute l:width  . 'wincmd |'
    silent! noautocmd execute l:window . 'wincmd w'
  finally
    let &l:lazyredraw = l:lazyredraw
  endtry
  let l:width = TWM#MasterWindow#GetWidth()
  let s:ratio = l:width / str2float(&columns)
endfunction
" ------------------------------------------------------------------------------
function! TWM#MasterWindow#GetWidth()
  return winwidth(1)
endfunction
" ------------------------------------------------------------------------------
function! TWM#MasterWindow#SetRatio(ratio)
  if     type(a:ratio) == type(0.0)
    let s:ratio = a:ratio
  elseif type(a:ratio) == type(0)
    let s:ratio = str2float(a:ratio)
  elseif type(a:ratio) == type('')
    let s:ratio = str2float(a:ratio)
  else
    throw
    \ "`a:ratio` should be one of types {`Float`, `Number`, `String`}, and only"
    \ ' '
    \ "values between \"0.0\" and \"1.0\" make sense."
  endif
  let s:ratio = s:Clamp(s:ratio, 0.0, 1.0)
  call TWM#MasterWindow#Resize()
endfunction
" ------------------------------------------------------------------------------
function! TWM#MasterWindow#GetRatio()
  return s:ratio
endfunction
" ------------------------------------------------------------------------------
function! TWM#MasterWindow#Resize()
  let l:width  = float2nr(round(s:ratio * &columns))
  let l:width  = s:Clamp(l:width, 0, l:width)
  let l:window = winnr()
  let  l:lazyredraw = &l:lazyredraw
  let &l:lazyredraw = 1
  try
    silent! noautocmd 1wincmd w
    silent! noautocmd execute l:width  . 'wincmd |'
    silent! noautocmd execute l:window . 'wincmd w'
  finally
    let &l:lazyredraw = l:lazyredraw
  endtry
endfunction
" ------------------------------------------------------------------------------
function! TWM#MasterWindow#Grow()
  let l:width = TWM#MasterWindow#GetWidth() + 1
  call TWM#MasterWindow#SetWidth(l:width)
endfunction
" ------------------------------------------------------------------------------
function! TWM#MasterWindow#Shrink()
  let l:width = TWM#MasterWindow#GetWidth() - 1
  call TWM#MasterWindow#SetWidth(l:width)
endfunction
" ------------------------------------------------------------------------------
function! TWM#MasterWindow#Maximize()
  call TWM#MasterWindow#SetRatio(1.0)
endfunction
" ------------------------------------------------------------------------------
function! TWM#MasterWindow#Minimize()
  call TWM#MasterWindow#SetRatio(0.0)
endfunction
" ==============================================================================
" }}} Public

" Private {{{
" ==============================================================================
function! s:Clamp(value, min, max)
  if a:value < a:min
    return a:min
  elseif a:value > a:max
    return a:max
  else
    return a:value
  endif
endfunction
" ==============================================================================
" }}} Private
" ==============================================================================
" }}} Functions

" Defaults {{{
" ==============================================================================
call TWM#MasterWindow#SetRatio(0.5)
" ==============================================================================
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
