" Preamble {{{
" ==============================================================================
"        File: Mappings.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

if exists('g:TWM#Mappings#autoload')
  finish
endif

if v:version < 700
  echoerr "TWM: Error: Minimum required version of Vim is \"7.0.0\"."
  finish
endif

let g:TWM#Mappings#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ==============================================================================
" Public {{{
" ==============================================================================
function! TWM#Mappings#Map()
  nmap <Leader>wr <Plug>TWM#Resize
  nmap <Leader>we <Plug>TWM#Equalize
  nmap <Leader>w= <Plug>TWM#Equalize
  nmap <Leader>ws <Plug>TWM#Stack
  nmap <Leader>wf <Plug>TWM#Focus
  nmap <Leader>wn <Plug>TWM#New
  nmap <Leader>wc <Plug>TWM#Close

  nmap <C-w>= <Plug>TWM#Equalize
  nmap <C-w>n <Plug>TWM#New
  nmap <C-w>c <Plug>TWM#Close

  nmap <C-Space> <Plug>TWM#Stack
  nmap <C-CR>    <Plug>TWM#Focus

  nmap <C-k> <Plug>TWM#RotateCW
  nmap <C-j> <Plug>TWM#RotateCCW

  nmap <Tab>   <Plug>TWM#SelectCW
  nmap <S-Tab> <Plug>TWM#SelectCCW

  nmap + <Plug>TWM#MasterWindow#Grow
  nmap - <Plug>TWM#MasterWindow#Shrink

  nmap <C-l> <Plug>TWM#MasterWindow#Maximize
  nmap <C-h> <Plug>TWM#MasterWindow#Minimize
endfunction
" ==============================================================================
" }}} Public
" ==============================================================================
" }}} Functions

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
