" Preamble {{{
" ==============================================================================
"        File: TWM.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

if exists('g:TWM#autoload')
  finish
endif

if v:version < 700
  echoerr "TWM: Error: Minimum required version of Vim is \"7.0.0\"."
  finish
endif

let g:TWM#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ==============================================================================
" Public {{{
" ==============================================================================
function! TWM#Resize()
  let  l:lazyredraw = &l:lazyredraw
  let &l:lazyredraw = 1
  try
    silent! noautocmd wincmd =
    silent! noautocmd call TWM#MasterWindow#Resize()
  finally
    let &l:lazyredraw = l:lazyredraw
  endtry
endfunction
" ------------------------------------------------------------------------------
function! TWM#Equalize()
  let  l:lazyredraw = &l:lazyredraw
  let &l:lazyredraw = 1
  try
    silent! noautocmd wincmd =
    silent! noautocmd call TWM#MasterWindow#SetRatio(0.5)
  finally
    let &l:lazyredraw = l:lazyredraw
  endtry
endfunction
" ------------------------------------------------------------------------------
function! TWM#Stack()
  let  l:lazyredraw = &l:lazyredraw
  let &l:lazyredraw = 1
  try
    let l:i = 1
    while l:i <= winnr('$')
      silent! noautocmd 1wincmd w
      silent! noautocmd  wincmd J
      let l:i += 1
    endwhile
    silent! 1wincmd w
  finally
    let &l:lazyredraw = l:lazyredraw
  endtry
endfunction
" ------------------------------------------------------------------------------
function! TWM#Focus()
  let  l:lazyredraw = &l:lazyredraw
  let &l:lazyredraw = 1
  try
    silent! noautocmd wincmd H
    silent! noautocmd call TWM#Stack()
    silent! noautocmd wincmd H
    silent! noautocmd call TWM#Resize()
  finally
    let &l:lazyredraw = l:lazyredraw
  endtry
endfunction
" ------------------------------------------------------------------------------
function! TWM#New()
  let  l:lazyredraw = &l:lazyredraw
  let &l:lazyredraw = 1
  try
    silent! noautocmd call TWM#Stack()
    vert topleft new
    silent! noautocmd call TWM#Resize()
  finally
    let &l:lazyredraw = l:lazyredraw
  endtry
endfunction
" ------------------------------------------------------------------------------
function! TWM#Close()
  let  l:lazyredraw = &l:lazyredraw
  let &l:lazyredraw = 1
  try
    if winnr() == 1
      close
      silent! noautocmd wincmd H
    else
      close
    endif
    silent! noautocmd call TWM#Resize()
  finally
    let &l:lazyredraw = l:lazyredraw
  endtry
endfunction
" ------------------------------------------------------------------------------
function! TWM#RotateCW()
  let  l:lazyredraw = &l:lazyredraw
  let &l:lazyredraw = 1
  try
    silent! noautocmd call TWM#Stack()
    silent! wincmd W
    silent! noautocmd wincmd H
    silent! noautocmd call TWM#Resize()
  finally
    let &l:lazyredraw = l:lazyredraw
  endtry
endfunction
" ------------------------------------------------------------------------------
function! TWM#RotateCCW()
  let  l:lazyredraw = &l:lazyredraw
  let &l:lazyredraw = 1
  try
    silent! noautocmd call TWM#Stack()
    silent! noautocmd wincmd J
    silent! 1wincmd w
    silent! noautocmd wincmd H
    silent! noautocmd call TWM#Resize()
  finally
    let &l:lazyredraw = l:lazyredraw
  endtry
endfunction
" ------------------------------------------------------------------------------
function! TWM#SelectCW()
  silent! wincmd w
endfunction
" ------------------------------------------------------------------------------
function! TWM#SelectCCW()
  silent! wincmd W
endfunction
" ==============================================================================
" }}} Public
" ==============================================================================
" }}} Functions

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
