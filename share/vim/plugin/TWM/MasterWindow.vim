" Preamble {{{
" ==============================================================================
"        File: MasterWindow.vim
" ------------------------------------------------------------------------------
"     Version: 0.0.0
" ------------------------------------------------------------------------------
"     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
" Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
" ------------------------------------------------------------------------------
"     License: This program is free software: you can redistribute it and/or
"              modify it under the terms of the GNU General Public License as
"              published by the Free Software Foundation, either version 3 of
"              the License, or (at your option) any later version.
"
"              This program is distributed in the hope that it will be useful,
"              but WITHOUT ANY WARRANTY; without even the implied warranty of
"              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
"              General Public License for more details.
"
"              You should have received a copy of the GNU General Public License
"              along with this program. If not, see
"              <http://www.gnu.org/licenses/>.
" ==============================================================================
" }}} Preamble

if exists('g:TWM#MasterWindow#plugin')
  finish
endif

if v:version < 700
  echoerr "TWM: Error: Minimum required version of Vim is \"7.0.0\"."
  finish
endif

let g:TWM#MasterWindow#plugin = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Plugs {{{
" ==============================================================================
" Normal {{{
" ==============================================================================
nnoremap <script> <silent> <unique> <Plug>TWM#MasterWindow#Grow
\ :call TWM#MasterWindow#Grow()<CR>

nnoremap <script> <silent> <unique> <Plug>TWM#MasterWindow#Shrink
\ :call TWM#MasterWindow#Shrink()<CR>

nnoremap <script> <silent> <unique> <Plug>TWM#MasterWindow#Maximize
\ :call TWM#MasterWindow#Maximize()<CR>

nnoremap <script> <silent> <unique> <Plug>TWM#MasterWindow#Minimize
\ :call TWM#MasterWindow#Minimize()<CR>
" ==============================================================================
" }}} Normal
" ==============================================================================
" }}} Plugs

" Hooks {{{
" ==============================================================================
augroup TWM#MasterWindow#Resize
  autocmd!
  autocmd WinEnter * call TWM#MasterWindow#Resize()
augroup END
" ==============================================================================
" }}} Hooks

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
